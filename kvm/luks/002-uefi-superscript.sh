#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "Ranking mirrors - This may take a few minutes."
echo "You may see errors during this step. It's normal."
echo "Please be patient."

reflector --protocol http --protocol https --country 'United States' --latest 25 --age 4 --sort rate --save /etc/pacman.d/mirrorlist

echo
echo "Done ranking mirrors!"

cp -r post /mnt/root/

echo
echo "~/superscript-post-uefi-luks-grub-kvm.sh && exit" > /mnt/root/.bashrc

chmod +x /mnt/root/*.sh

pacstrap /mnt base base-devel linux-lts linux-lts-headers e2fsprogs btrfs-progs nano

genfstab -pU /mnt | tee -a /mnt/etc/fstab

echo "#tmpfs /tmp tmpfs defaults,noatime,mode=1777 0 0" >> /mnt/etc/fstab
echo
echo "Ready to arch-chroot and finish setup?"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."
echo
arch-chroot /mnt /bin/bash
