#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sed -i 's/^#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen

locale-gen

echo LANG=en_US.UTF-8 > /etc/locale.conf

export LANG=en_US.UTF-8

ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime

hwclock --systohc --utc

echo
echo "What should the hostname be?"

read input

echo $input > /etc/hostname

mv /etc/pacman.conf /etc/pacman.bak

cp /root/pacman.conf /etc/pacman.conf

pacman -Sy

echo
echo "Set root password"

passwd

echo
echo "What is your username?"

read input1

useradd -m -G wheel,storage,power -s /bin/bash $input1

echo "Set your password."
passwd $input1

mv /etc/sudoers /etc/sudoers.bak

cp /root/sudoers /etc/sudoers

mv /root/desktops.sh /home/$input1/

chown $input1:$input1 /home/$input1/desktops.sh

chmod +x /home/$input1/desktops.sh

chown $input1:$input1 /home/$input1
	
pacman -S --needed --noconfirm bash-completion efibootmgr os-prober
pacman -S --needed --noconfirm git xdg-user-dirs networkmanager

# Former base group
pacman -S --needed --noconfirm bash bzip2 cryptsetup device-mapper dhcpcd e2fsprogs findutils
pacman -S --needed --noconfirm gawk gcc-libs gettext glibc inetutils iproute2 iputils
pacman -S --needed --noconfirm jfsutils linux-firmware logrotate lvm2 man-db mdadm nano
pacman -S --needed --noconfirm netctl pacman pciutils perl procps-ng psmisc reiserfsprogs
pacman -S --needed --noconfirm s-nail shadow sysfsutils systemd-sysvcompat texinfo grub
pacman -S --needed --noconfirm usbutils util-linux vi xfsprogs intel-ucode dialog f2fs-tools

systemctl enable NetworkManager.service

mv /etc/mkinitcpio.conf /etc/mkinitcpio.bak

cp /root/mkinitcpio-grub.conf /etc/mkinitcpio.conf

mkinitcpio -p linux-lts

# Testing
#os-prober
#efibootmgr

# grub-install UEFI
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB --modules="part_gpt part_msdos lvm luks"

mv /etc/default/grub /etc/default/old.grub

cp /root/grub-kvm-luks /etc/default/grub

grub-mkconfig -o /boot/grub/grub.cfg

read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."
