#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "WARNING!!! This script will *destroy everything* on /dev/vda"
echo "Do NOT use this script if you need to do manual partitioning!"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

mkfs.vfat -F32 /dev/vda1

mkfs.ext2 /dev/vda2

cryptsetup -c aes-xts-plain64 -y --use-random luksFormat /dev/vda3

cryptsetup luksOpen /dev/vda3 luks

pvcreate /dev/mapper/luks

vgcreate vg0 /dev/mapper/luks

lvcreate --size 2G vg0 --name swap

lvcreate -l +100%FREE vg0 --name root

mkfs.btrfs /dev/mapper/vg0-root

mkswap /dev/mapper/vg0-swap

mount /dev/mapper/vg0-root /mnt

swapon /dev/mapper/vg0-swap

mkdir /mnt/boot

mount /dev/vda2 /mnt/boot

mkdir /mnt/boot/efi

mount /dev/vda1 /mnt/boot/efi

echo
echo "Do these mount points look right?"
echo
mount | grep /mnt
echo
echo "Here is the UUID for your root parition."
blkid | grep /dev/vda3
echo
